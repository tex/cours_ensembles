\documentclass[12pt,a4paper]{report}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{pifont}
\usepackage{alltt}
\usepackage{multicol}
\usepackage{multirow}
\usepackage{setspace}
\usepackage{color}
\usepackage{lscape}
\usepackage{enumitem}
\usepackage[left=2cm,right=2cm,top=2cm,bottom=2cm]{geometry}
\pagestyle{empty}

\newcommand{\diff}{\mathrm{d}}
\newcommand{\e}{\mathrm{e}}
\newcommand{\im}{\mathbf{i}}
\newcommand{\jm}{\mathbf{j}}
\newcommand{\red}{\textcolor{red}}
\newcommand{\blue}{\textcolor{blue}}
\newcommand{\green}{\textcolor{green}}
\newcommand{\lensent}{[\![}
\newcommand{\rensent}{]\!]}

\title{Ensembles}
\begin{document}

\chapter{Ensembles, applications, relations}
\begin{itemize}[label=$\bullet$]
	\item \textbf{Proposition :} la plupart des résultats;
	\item \textbf{Théorème :} les résultats les plus fondamentaux;
	\item \textbf{Corollaire :} les conséquences des résultats précédents;
	\item \textbf{Lemme :} certains résultats préliminaires utiles pour la suite, mais à l'intérêt limité.
\end{itemize}
\medskip

\section{Assertions}
\subsection{Assertions}
\textit{Exemples}
\begin{enumerate}
	\item "2 est un en entier impair" est une assertion (fausse);
	\item "$(1000+1)^2=1000^2+2000+1$" est une assertion (vraie);
	\item "$1=2+$" n'est pas une assertion.
\end{enumerate}
On dit que deux assertions sont logiquement équivalentes, si elles ont la même valeur de vérité.

\subsection{Connecteurs}
A partir d'un certain nombres d'assertions, on peut en fabriquer de nouvelles en utilisant des \textit{connecteurs}.
\paragraph*{Connecteurs élémentaires}
Soient $P$ et $Q$ deux assertions, on définit :
\begin{itemize}[label=$\bullet$]
	\item $(NON\; P)$ qui est vraie lorsque $P$ est fausse, et fausse sinon,
	\item $(P\; ET\; Q)$ qui est vraie lorsque les deux assertions $P$ et $Q$ sont vraies, et fausse sinon,
	\item $(P\; OU\; Q)$ qui est vraie lorsqu'au moins une des deux assertions est vraie, et fausse sinon.
\end{itemize} \newpage
Par la suite, on définit $\begin{array}{l}
	(NON\; P)$ par $\neg P \\
	(P\; ET\; Q)$ par $(P\wedge Q) \\
	(P\; OU\; Q)$ par $(P\vee Q) \\
\end{array}$ \medskip
Les valeurs de vérité de ces nouvelles assertions peuvent être représentées par les tableaux suivants, appelés \textit{tables de vérité} :
\begin{center}
\begin{tabular}{|c|c|c|c|c|}
	\hline
	$\mathbf{P}$ & $\mathbf{Q}$ & $\neg\mathbf{P}$ & $\mathbf{P}\wedge\mathbf{Q}$ & $\mathbf{P}\vee\mathbf{Q}$ \\ \hline
	V & V & F & V & V \\ \hline
	V & F & F & F & V \\ \hline
	F & V & V & F & V \\ \hline
	F & F & V & F & F \\ \hline
\end{tabular}
\end{center}
\paragraph{Implication, équivalence}
Soient $P$ et $Q$ deux assertions, on définit alors les assertions implication et équivalence par :
\\ \\ \fbox{
\parbox{15cm}{
\setlength{\parskip}{.05cm}
\[\textcolor{red}{P\Rightarrow Q\; :\; (\neg P)\vee Q}\]
\[P\Leftrightarrow Q\; :\; (P\Rightarrow Q)\wedge(Q\Rightarrow P)\] }}
\medskip
\medskip
\medskip \\
Ces assertions vérifient la table de vérité suivante :
\begin{center}
\begin{tabular}{|c|c|c|c|}
	\hline
	$\mathbf{P}$ & $\mathbf{Q}$ & $\mathbf{P}\Rightarrow\mathbf{Q}$ & $\mathbf{P}\Leftrightarrow\mathbf{Q}$ \\ \hline
	V & V & V & V \\ \hline
	V & F & F & F \\ \hline
	F & V & V & F \\ \hline
	F & F & V & V \\ \hline
\end{tabular}
\end{center}
\subsection{Méthodes de démonstration}
Dans une théorie mathématique : \begin{itemize}[label=$\bullet$]
	\item un \textit{axiome} est une assertion que l'on pose vraie \textit{a priori};
	\item un \textit{théorème} est une assertion que l'on peut déduire d'axiomes ou d'autres théorèmes.
\end{itemize}
\medskip
$\begin{array}{rl}
	$\textbf{Modus ponens :}$ & $ si on a $ P\;\wedge\;(P\Rightarrow Q)$ alors on a $Q. \\
	$\textbf{Transitivité de l'implication :}$ & $ si on a $ (P\Rightarrow Q)\;\wedge\;(Q\Rightarrow R)$ alors on a $P\Rightarrow R. \\
	$\textbf{Contraposée :}$ & $ on a $ P\Rightarrow Q$ ssi on a $\neg Q\Rightarrow\neg P \\
	$\textbf{Disjonction des cas :}$ & $ si on a $(P\vee Q)\wedge(P\Rightarrow R)\wedge(Q\Rightarrow R)$ alors on a $R. \\
	$\textbf{Raisonnement par l'absurde :}$ & $ si on a $(\neg P\Rightarrow Q)\wedge(\neg P\Rightarrow\neg Q)$ alors on a $P. \\
\end{array}$ \newpage
\section{Ensembles, prédicats}
\subsection{Généralités}
Deux nouvelles assertions : \begin{itemize}[label=$\bullet$]
	\item $a\in E$ qui se lit : \textcolor{blue}{$a$ appartient à $E$} ou \textcolor{blue}{$a$ est un élément de $E$}, et dont la négation s'écrit $a\notin E$.
	\item $E\subset F$ qui se lit : \textcolor{blue}{$E$ inclus dans $F$} et qui signifie que tout élément de $E$ est aussi élément de $F$. Sa négation s'écrit $E\not\subset F$.
\end{itemize}
\medskip
Deux ensembles $E$ et $F$ sont égaux ($E=F$) s'ils ont les mêmes éléments, cad si l'on a simultanément $E\subset F$ et $F\subset E$. \\
On admet les résultats suivants : \begin{itemize}[label=$\bullet$]
	\item Il existe un ensemble, appelé \textit{ensemble vide} noté $\o$, qui ne contient aucun élément. Il est inclus dans tout ensemble.
	\item Si $E$ est un ensemble, il existe un ensemble, appelé \textit{ensemble des parties} de $E$ et noté $\mathcal{P}(E)$, dont les éléments sont tous les ensembles inclus dans $E$. Donc, par définition, pour tout ensemble $F$ : 
\end{itemize}\begin{center} \fbox{
\parbox{5cm}{
\setlength{\parskip}{.02cm}
	\[F\in\mathcal{P}(E)\;\Longleftrightarrow\; F\subset E\] }} \end{center}
\textit{Exemple} Si $E=\lbrace a,b,c\rbrace$ est l'ensemble dont les éléments sont $a$, $b$ et $c$, l'ensemble de ses parties est :
\[\mathcal{P}(E)=\lbrace\o,\lbrace a\rbrace,\lbrace b\rbrace,\lbrace c\rbrace,\lbrace a,b\rbrace,\lbrace a,c\rbrace,\lbrace b,c\rbrace,E\rbrace\]
\subsection{Prédicats}
\textit{Exemples} \begin{enumerate}
	\item "$x$ est pair" n'est pas une assertion, puisqu'on ne peut pas lui donner de valeur de vérité tant qu'on ne connait pas $x$. C'est ce qu'on appelle un prédicat sur $\mathbb{N}$ : il dépend d'une variable $x$, et lorsqu'on remplace $x$ par une valeur entière, on obtient une assertion.
	\item De même, "$x^2+1>0$" est un prédicat sur $\mathbb{R}$ mais pas sur $\mathbb{C}$ tant que l'on n'a pas défini de relation $>$ sur les complexes.
	\item "$x+y^2=0$" est un prédicat à deux variables réelles.
	\item Lorsqu'on substitue à $x$, dans le prédicat précédent, le réel $1$, on obtient le prédicat à une seule variable "$1+y^2=0$".
\end{enumerate}
Un \textit{prédicat} est ainsi un énoncé $A(x,t,\cdots)$ dépendant de variables $x,y,\cdots$ tel que, lorsqu'on substitue à ces variables des éléments de certains ensembles, on obtienne une assertion. \newpage

\subsection{Quantificateurs}
A partir d'un prédicat $A(x)$ à une variable $x$ dans un ensemble $E$, on peut construire trois assertions : \begin{itemize}[label=$\bullet$]
	\item $\forall x\in E,\; A(x)$ qui se lit : \textit{quel que soit l'élément $x$ de $E$, $A(x)$}.
	\item $\exists x\in E:\; A(x)$ qui se lit : \textit{il existe un élément $x$ de $E$ tel que $A(x)$}.
	\item Le symbole $\forall$ est appelé \textbf{quantificateur universel}.
	\item Le symbole $\exists$ est appelé \textbf{quantificateur existentiel}.
\end{itemize}
\subsection{Négation de quantificateurs}
\textcolor{red}{
\[\neg(\forall x\in E,\; A(x))\;\Longleftrightarrow\;\exists x\in E:\;\neg A(x)\]
\[\neg(\exists x\in E:\; A(x))\;\Longleftrightarrow\;\forall x\in E,\;\neg A(x)\]
}
\subsection{Sous-ensembles définis par un prédicat}
Si $P$ est un prédicat à une variable et si $E$ est un ensemble, on peut définir la partie $E$ constituée des éléments de $E$ vérifiant $P$. On la note :
\[F=\lbrace x\in E\; |\; P(x)\rbrace\]
Cette partie est donc caractérisée par l'équivalence suivante :
\[\forall x\in E,\; x\in F\Longleftrightarrow P(x)\]
\subsection{Opérations sur les parties}
Soient $A$ et $B$ deux parties d'un ensemble $E$. On appelle :
\begin{itemize}
	\item \textit{intersection} de $A$ et de $B$, la parte de $E$ définie par :
	\[A\cap B=\lbrace x\in E\; |\; x\in A\wedge x\in B\rbrace\]
	\item \textit{réunion} de $A$ et $B$, la partie de $E$ définie par :
	\[A\\cup B=\lbrace x\in E\; |\; x\in A\vee x\in B\rbrace\]
	\item \textit{différence} de $A$ et de $B$, la partie de $E$ définie par :
	\[A\setminus B=\lbrace x\in A\; |\; x\notin B\rbrace\]
	\item \textit{complémentaire} de $A$ dans $E$, la différence :
	\[\complement_EA=E\setminus A\]
\end{itemize} \newpage
\subsection{Couples, produit cartésien}
\begin{itemize}
	\item A partir de deux éléments $a$ et $b$, on peut construire le \textit{couple} $(a,b)$, avec la propriété fondamentale suivante : \[(a,b)=(a',b')\Longleftrightarrow(a=a'\;\wedge\; b=b')\]
	Étant donnés deux ensembles $A$ et $B$, l'ensemble des couples de la forme $(a,b)$, avec $a\in A$ et $b\in B$ est appelé \textit{\textbf{produit cartésien}} de $A$ par $B$ et se note \textcolor{red}{$A\times B$}. \\
	On a donc : \[A\times B=\lbrace(a,b)\; |\; a\in A\;\wedge\; b\in B\rbrace\]
	Lorsque $A=B$, le produit cartésien $A\times B$ se note aussi $A^2$.
	\item De même, on peut définir la notion de \textit{triplet} $(a,b,c)$ vérifiant la propriété :
	\[(a,b,c)=(a',b',c')\Longleftrightarrow(a=a'\wedge b=b'\wedge c=c')\]
	ainsi que le produit : \[A\times B\times C=\lbrace(a,b,c)\;|\; a\in A\wedge b\in B\wedge c\in C\rbrace\]
	Le produit $A\times A\times A$ se note aussi $A^3$.
	\item Plus généralement, pour $n\in\mathbb{N}^*$, on peut définir la notion de $n$\textit{-uplet} ou de $n$\textit{-liste} $(a_1,a_2,\ldots,a_n)$ ainsi que l'ensemble : \[A_1\times A_2\times\ldots\times A_n=\lbrace(a_1,a_2,\ldots,a_n)\;|\; a_1\in A_1,a_2\in A_2,\ldots,a_n\in A_n\rbrace\]
	Lorsque $A_1=A_2=\cdots=A_n=A$, le produit $A_1\times A_2\times\cdots\times A_n$ est noté $A^n$. \\
	En particulier lorsque $n=1$, on identifie la 1-liste$(a)$ avec son unique élément $a$. On a ainsi $A^1=A$. \\
	Par convention, il existe une unique 0-liste, appelée liste vide. On note alors $A^0$ l'ensemble qui contient comme unique élément la liste vide. 
\end{itemize} \newpage

\section{Applications}
Dans toute cette section, $E$ et $F$ désignent des ensembles quelconques.
\subsection{Définitions}
\subsubsection*{Définition 1}
\fbox{
\parbox{15cm}{
\setlength{\parskip}{.05cm}
\begin{itemize}
	\item On appelle \textit{graphe} de $E$ vers $F$ toute partie du produit cartésien $E\times F$.
	\item Une \textit{application} (ou \textit{fonction}) est un triplet $u=(E,F,\Gamma)$ où $\Gamma$ est un graphe de $E$ vers $F$ tel que, pour tout $x\in E$, il existe un unique $y\in F$ tel que $(x,y)\in\Gamma$. \\
	On dit aussi que $u$ est une application de $E$ dans $F$ ou de $E$ vers $F$.
\end{itemize} }} \medskip \\
Avec les notations précédentes : \begin{itemize}
	\item $E$ est appelé l'\textit{ensemble de départ} ou \textit{ensemble de définition} de $u$,
	\item $F$ est l'\textit{ensemble d'arrivée} de $u$,
	\item pour $x\in E$, l'unique $y\in F$ tel que $(x,y)\in\Gamma$ s'appelle \textit{image} de $x$ par $u$ et se note $u(x)$,
	\item pour $y\in F$, tout $x\in E$ tel que $y=u(x)$ est appelé \textit{antécédent} de $y$,
	\item le \textit{graphe} $\Gamma$ de $u$, est égal à $\lbrace(x,u(x))\;|\; x\in E\rbrace$,
	\item l'ensemble : \[\lbrace y\in F\;|\;\exists x\in E:\; y=u(x)\rbrace\text{  noté  }\lbrace u(x)\;|\; x\in E\rbrace\] est l'\textit{ensemble image} de $u$ ; c'est une partie de $F$,
	\item l'application $u$ se note : \begin{center}$\begin{array}{cccccccc}
	E\stackrel{u}{\longrightarrow}F & \text{ ou } & u:\; E\longrightarrow F & \text{ ou } & u: & E & \longrightarrow & F \\
	&&&&& x & \longmapsto & u(x). \\
	\end{array}$
	\end{center}
\end{itemize}
\paragraph*{Remarque}
L'application $f:\; E\longrightarrow E$ est appelée identité de $E$ et se note $\mathrm{Id}_E$.
\paragraph*{Égalité de deux applications} Comme conséquence de la définition, on déduit que légalité de deux applications $u$ et $v$ signifie : \begin{enumerate}
	\item l'égalité des ensembles de départ,
	\item l'égalité des ensembles d'arrivée,
	\item l'égalité $u(x)=v(x)$ pour tout $x$ appartenant à l'ensemble de départ commun.
\end{enumerate}
\newpage
\subsubsection*{Définition 2} \fbox{
\parbox{15cm}{
\setlength{\parskip}{.05cm} Soit $u$ une application de $E$ dans $F$. \begin{itemize}
	\item Si $A$ est une partie de $E$, la \textit{restriction} de $u$ à $A$, notée $u_{|A}$, est l'application de $A$ dans $F$ définie par : \[\forall x\in A,\; u_{|A}(x)=u(x)\]
	\item On appelle \textit{prolongement} de $u$ toute application $v$ définie sur un ensemble $A$ contenant $E$ et vérifiant : \[\forall x\in E,\; v(x)=u(x)\]
\end{itemize} }}
\subsection{Injectivité, surjectivité, bijectivité}
\subsubsection*{Définition 3} \fbox{
\parbox{15cm}{
\setlength{\parskip}{.05cm} On dit qu'une application $u$ de $E$ dans $F$ est une \textit{injection} ou est \textit{injective} si elle vérifie l'une des trois propriétés équivalentes suivantes : \begin{enumerate}
	\item[(\textit{i})] Tout élément de $F$ a au plus un antécédent par $u$.
	\item[(\textit{ii})] Pour tout $y\in F$, l'équation $u(x)=y$ possède au plus une solution.
	\item[(\textit{iii})] $\forall(x_1,x_2)\in E^2,\; u(x_1)=u(x_2)\Longrightarrow x_1=x_2$.
\end{enumerate} }} \medskip
\subsubsection*{Définition 4}\fbox{
\parbox{15cm}{
\setlength{\parskip}{.05cm} On dit qu'une application $u$ de $E$ dans $F$ est une \textit{surjection} ou est \textit{surjective} si elle vérifie l'une des trois propriétés équivalentes suivantes : \begin{enumerate}
	\item[(\textit{i})] Tout élément de $F$ a au moins un antécédent par $u$.
	\item[(\textit{ii})] Pour tout $y\in F$, l'équation $u(x)=y$ possède au moins une solution.
	\item[(\textit{iii})] $\forall y\in F,\;\exists x\in E:\; y=u(x)$.
\end{enumerate} }} \medskip
\subsubsection*{Définition 5}\fbox{
\parbox{15cm}{
\setlength{\parskip}{.05cm} On dit qu'une application $u$ de $E$ dans $F$ est une \textit{bijection} ou est \textit{bijective} si elle est injective et surjective, c'est-à-dire si elle vérifie l'une des deux propriétés équivalentes suivantes : \begin{enumerate}
	\item[(\textit{i})] Tout élément de $F$ a un et un seul antécédent par $u$.
	\item[(\textit{ii})] Pour tout $y\in F$, l'équation $u(x)=y$ possède une unique solution.
\end{enumerate} }} \medskip
\subsubsection*{Définition 6}\fbox{
\parbox{15cm}{
\setlength{\parskip}{.05cm} Une application bijective de $E$ sur $E$ est encore appelée \textit{permutation} de $E$.\\ L'ensemble des permutations de $E$ est habituellement noté $\mathcal{S}(E)$ }}
\subsection{Composition d'applications} Soient $E$, $F$, $G$ et $H$ quatre ensembles.
\subsubsection*{Définition 7} \fbox{
\parbox{15cm}{
\setlength{\parskip}{.05cm} Si $u\in\mathcal{F}(E,G)$ et $v\in\mathcal{F}(F,G)$, l'application $x\mapsto v(u(x))$ définie sur $E$ et à valeurs dans $G$ est appelée \textit{composée} des applications $v$ et $u$ ; on la note $v\circ u$. }} \medskip
\subsubsection*{Proposition 1} Étant données trois applications $E\stackrel{u}{\longrightarrow}F\stackrel{v}{\longrightarrow}G\stackrel{w}{\longrightarrow}H$, on a : \[w\circ(v\circ u)=(w\circ v)\circ u\] \medskip
\subsubsection*{Proposition 2} Soient $u\in\mathcal{F}(E,F)$ et $v\in\mathcal{F}(F,G)$. \begin{enumerate}
	\item Si $u$ et $v$ sont injectives, alors $v\circ u$ est injective.
	\item Si $u$ et $v$ sont surjectives, alors $v\circ u$ est surjective.
	\item Si $u$ et $v$ sont bijectives, alors $v\circ u$ est bijective.
\end{enumerate}
\subsection{Application réciproque}
\subsubsection*{Définition 8}\fbox{
\parbox{15cm}{
\setlength{\parskip}{.05cm} Si $u$ est une application bijective de $E$ dans $F$, alors l'application de $F$ dans $E$ qui associe à tout élément de $F$ son unique antécédent dans $E$ s'appelle \textit{application réciproque} de $u$ et se note $u^{-1}$. \\ On a donc : \[\forall(x,y)\in E\times F,\; y=u(x)\Longleftrightarrow x=u^{-1}(y)\] }} \medskip
\subsubsection*{Proposition 3} Si $u$ est une application bijective de $E$ ans $F$, on a : \begin{center} $\begin{array}{lcr}
u^{-1}\circ u=\mathrm{Id}_E & \text{ et } & u\circ u^{-1}=\mathrm{Id}_F \\
\end{array}$\end{center} \medskip
\subsubsection*{Proposition 4} Si $u\in\mathcal{F}(E,F)$ et $v\in\mathcal{F}(F,E)$ sont deux applications vérifiant $u\circ v=\mathrm{Id}_F$ et $v\circ u=\mathrm{Id}_E$, alors elles sont toutes deux bijectives et réciproques l'une de l'autre. \newpage
\paragraph*{Corollaire} Si $u$ est une bijection de $E$ dans $F$, alors $u^{-1}$ est une bijection de $F$ dans $E$ et $(u^{-1})^{-1}=u$.
\subsubsection*{Proposition 5} Soient $E$, $F$ et $G$ trois ensembles. Si $E\stackrel{u}{\longrightarrow}F\stackrel{v}{\longrightarrow}G$ sont deux applications bijectives alors $v\circ u$ est bijective et : \[(v\circ u)^{-1}=u^{-1}\circ v^{-1}\]
\subsection{Images directes, images réciproques}
\subsubsection*{Définition 9} \fbox{
\parbox{15cm}{
\setlength{\parskip}{.05cm} Soient $u$ une application de $E$ dans $F$ ainsi que $A$ une partie de $E$ et $B$ une partie de $F$. On appelle : \begin{itemize}
	\item \textit{image (directe)} de $A$ par $u$, l'ensemble : \[u(A)=\lbrace y\;|\;\exists x\in A:\; y=u(x)\rbrace=\lbrace u(x)\;|\;x\in A\rbrace\]
	\item \textit{image réciproque} de $B$ par $u$, l'ensemble : \[u^{-1}(B)=\lbrace x\in E\;|\; u(x)\in B\rbrace\]
\end{itemize} }}
\subsection{Familles}
\subsubsection*{Définition 10} \fbox{
\parbox{15cm}{
\setlength{\parskip}{.05cm}  Étant donnés deux ensembles $I$ et $E$, on appelle \textit{famille} d'éléments de $E$ \textit{indexée} par $I$ toute application de $I$ dans $E$ : \[(x(i))_{i\in I}\] }} \newpage

\section{Relations d'ordre et résumé}
La relation $\mathcal{R}$ est $\textbf{réflexive}$ si $(\forall x\in E)(x\mathcal{R}x)$ \\
$\text{ }\;\;\;\;$La relation $\mathcal{R}$ est $\textbf{symétrique}$ si $(\forall(x,y)\in E^2)(x\mathcal{R}y\,\Rightarrow\,y\mathcal{R}x)$ \\
$\text{ }\;\;\;\;$La relation $\mathcal{R}$ est $\textbf{antisymétrique}$ si $(\forall(x,y)\in E^2)(x\mathcal{R}y$ et $y\mathcal{R}x\,\Rightarrow\,x=y)$ \\
$\text{ }\;\;\;\;$La relation $\mathcal{R}$ est $\textbf{transitive}$ si $(\forall(x,y,z)\in E^3)(x\mathcal{R}y$ et $y\mathcal{R}z\,\Rightarrow\, x\mathcal{R}z)$ \\

\medskip

Une relation d'équivalence est une relation $\textbf{réflexive}$, $\textbf{symétrique}$, et $\textbf{transitive}$. \\
$\text{ }\;\;\;\;$Il en découle les classes d'équivalence :
\begin{center}
$\mathcal{C}_x=\lbrace y\in E\,\vert\,x\mathcal{R}y\rbrace$
\end{center}
$\text{ }\;\;\;\;$Une relation d'ordre est une relation $\textbf{réflexive}$, $\textbf{antisymétrique}$, et $\textbf{transitive}$. \\
$\text{ }\;\;\;\;$Elle est d'ordre total si $(\forall(x,y)\in E^2)(x\mathcal{R}y$ ou $y\mathcal{R}x)$, d'ordre partiel sinon. \\

\medskip

$m$ est un minorant si $(\forall x\in F)(m\mathcal{R}x)$ avec $m\in E$ \\
$\text{ }\;\;\;\;M$ est un majorant si $(\forall x\in F)(x\mathcal{R}M)$ avec $M\in E$ \\
$\text{ }\;\;\;\;\mathrm{min}(F)$ est le plus grand des minorants appartenant à F. \\
$\text{ }\;\;\;\;\mathrm{max}(F)$ est le plus petit des majorants appartenant à F. \\
$\text{ }\;\;\;\;\mathrm{inf}(F)$ est le plus grand des minorants. \\
$\text{ }\;\;\;\;\mathrm{sup}(F)$ est le plus petit des majorants. \\

\medskip

$\mathcal{P}(E)=\lbrace X,\,X\subset E\rbrace$ \\

\medskip

$A\cap B=\lbrace x\in E\,\vert\,(x\in A)$ et $(x\in B)\rbrace$ \\
$\text{ }\;\;\;\;A\cup B=\lbrace x\in E\,\vert\,(x\in A)$ ou $(x\in B)\rbrace$ \\
$\text{ }\;\;\;\;A\setminus B=\lbrace x\in E\,\vert\,(x\in A)$ et $(x\notin B)\rbrace$ \\
$\text{ }\;\;\;\;A\Delta B=\lbrace x\in E\,\vert\,(x\in A\cup B)$ et $(x\notin A\cap B)\rbrace$ \\

\medskip

$\complement_E A=E\setminus A=\lbrace x\in E\,\vert\, x\notin A\rbrace$ \\

\medskip

$(P\,\Rightarrow\,Q)\,\Leftrightarrow\,(\neg P$ ou $Q)$ \\

\medskip

L'application $f$ est $\textbf{injective}$ si $(\forall(x,x')\in E^2)(f(x)=f(x')\,\Rightarrow\, x=x')$ \\
$\text{ }\;\;\;\;$L'application $f$ est $\textbf{surjective}$ si $(\forall y\in F)(\exists x\in E)(y=f(x))$ \\
$\text{ }\;\;\;\;$L'application $f$ est $\textbf{bijective}$ si elle est $\textbf{injective}$ et $\textbf{surjective}$. \\

\medskip

La suite $(u_n)_{n\in\mathbb{N}}$ est $\textbf{convergente}$ si $(\exists l\in\mathbb{R})(\forall\varepsilon >0)(\exists n_\varepsilon \in\mathbb{N})(\forall n\in\mathbb{N})(n\geq n_\varepsilon\,\Rightarrow\,\vert u_n-l\vert\leq\varepsilon)$ \\
$\text{ }\;\;\;\;$La suite $(u_n)_{n\in\mathbb{N}}$ est $\textbf{divergente}$ si $(\forall l\in\mathbb{R})(\exists\varepsilon >0)(\forall n\in\mathbb{N})(\exists p_{n,\varepsilon}\in\mathbb{N})(p_{n,\varepsilon}\geq n$ et $\vert u_{p_{n,\varepsilon}}-l\vert>\varepsilon)$ \\
$\text{ }\;\;\;\;$Une $\textbf{suite de Cauchy}$ vérifie $(\forall\varepsilon > 0)(\exists n_\varepsilon\in\mathbb{N})(\forall(n,p)\in\mathbb{N}^2)(n\geq n_\varepsilon\,\Rightarrow\,\vert u_{n+p}-u_n\vert\leq\varepsilon)$ \\

\medskip

$\mathrm{Card}(\mathbb{N})=\mathrm{Card}(\mathbb{Z})=\mathrm{Card}(\mathbb{D})=\mathrm{Card}(\mathbb{Q})=\aleph_0\;\;\;\;\;$ (Infini dénombrable) \\
$\text{ }\;\;\;\;\mathrm{Card}(\mathbb{R})=\mathfrak{c}=2^{\aleph_0}=\aleph_1\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;$ (Infini indénombrable) \\
\end{document}